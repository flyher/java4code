// C继承A,B接口
// 如果用类来实现C，则需要覆写这A,B,C的三个方法
interface C extends A, B {
  public void funC();
}