import java.io.FileReader;
import java.io.FileWriter;

public class TestFile {
  public static void main(String[] args) {
    FileReader fr = null;
    FileWriter fw = null;
    try {
      fr = new FileReader("./from.txt");
      fw = new FileWriter("./to.txt");
      char[] buffer = new char[100];
      int temp = fr.read(buffer, 0, buffer.length);
      // for (int i = 0; i < buffer.length; i++) {
      // System.out.println(buffer[i]);
      // }
      fw.write(buffer, 0, temp);

    } catch (Exception e) {
      // TODO: handle exception
      System.out.println(e);
    } finally {
      try {
        fr.close();
        fw.close();
      } catch (Exception e) {
        //TODO: handle exception
        System.out.println(e);
      }
    }
  }
}