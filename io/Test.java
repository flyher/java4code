import java.io.FileOutputStream;

// 1.引入包
import java.io.*;
 class Test {
  public static void main(String[] args) {
    // 2.声明输入流引用
    FileInputStream fis= null;
    // 声明输出流的引用
    FileOutputStream fos=null;
    try {
      // 3.生成代表输入流的对象
      fis=new FileInputStream("./from.txt");
      // 生成代表输出流的对象
      fos=new FileOutputStream("./to.txt");
      // 4.生成一个字节数组
      byte [] buffer = new byte[100];
      // 5.调用输入流对象的read方法，读取
      // 返回值是int类型
      int temp = fis.read(buffer,0,buffer.length);
      // for (int i = 0; i < buffer.length; i++) {
      //   System.out.println(buffer[i]);
      // }
      fos.write(buffer,0,temp);

      // 调用一个String独享的trim方法将会去掉字符串首位空格和空字符
      String str=new String(buffer);
      System.out.println(str.trim());
    } catch (Exception e) {
      //TODO: handle exception
    }
  }
}