import java.io.BufferedReader;
import java.io.FileReader;

class TestLine {
  public static void main(String[] args) {
    FileReader fr = null;
    BufferedReader br = null;
    try {
      fr = new FileReader("./from.txt");
      br =new BufferedReader(fr);
      // String line = br.readLine();
      // System.out.println(line);
      String line=null;
      while(true){
        line=br.readLine();
        if(line == null){
          break;
        }
        System.out.println(line);
      }
    } catch (Exception e) {
      // TODO: handle exception
      System.out.println(e);
    } finally {
      try {
        br.close();
        fr.close();
      } catch (Exception e) {
        //TODO: handle exception
        System.out.println(e);
      }
    }
  }
}