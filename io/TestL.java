import java.io.FileInputStream;
import java.io.FileOutputStream;

// Test read large txt
// read a txt file by part
class TestL {
  public static void main(String[] args) {
    FileInputStream fis =null;
    FileOutputStream fos=null;
    try {
      fis = new FileInputStream("./news.txt");
      fos = new FileOutputStream("./to.txt");
      // 小写的byte
      // 每次读取1024个字节
      byte [] buffer=new byte[1024];
      // for (int i = 0; i < max; i++) {
      //   int temp=fis.read(buffer,0,buffer);
      //   if(temp==-1){
      //     break;
      //   }
      // } 
      while(true){
        int temp = fis.read(buffer,0,buffer.length);
        if(temp==-1){
          break;
        }
        fos.write(buffer,0,temp);
      }

    } catch (Exception e) {
      //TODO: handle exception
    } finally{
      // TestL.java:34: error: unreported exception IOException; must be caught or declared to be thrown
      // fis.close();
      //          ^
      // TestL.java:35: error: unreported exception IOException; must be caught or declared to be thrown
      try {
        fos.close();
        fis.close();
      } catch (Exception e) {
        //TODO: handle exception
      }

    }
  }
}