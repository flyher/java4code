// class A {
//      int i;
//     class B {
//         int j;
//         int funB() {
//             // B可以使用外部类成员变量i，但并不表示B拥有成员变量i
//             int result = i+j;
//             return result;
//         }
//     }
// }

interface A {
    public void doSomeThing();
}