class Test {
    public static void main(String args[]){
        // 1
        // // 生成外部类对象
        // A a=new A();
        
        // // 生成内部类对象
        // // 先生成一个外部类的对象 new A()
        // // .new B()  来生成外部类的对象
        // // option 1
        // // A.B b = new A().new B();
        // // option 2
        // A.B b = a.new B();
        // a.i = 3;
        // b.j = 1;
        // int result= b.funB();
        // System.out.println(result);

        // 2
        // AImpl al = new AImpl();
        // A a = al;

        B b = new B();
        // b.fun(a);
        // 匿名函数
        b.fun(new A(){
            public void dosomething(){
                System.out.println('匿名函数');
            }
        });
    }
}