class Test {
  public static void main(String[] args) {
    
    // 实现了bird接口
    // Chicken chicken = new Chicken();
    // Bird bird = chicken;
    // bird.swim();
    // bird.fly();

    // 同时实现了animal,bird接口
    // Chicken chicken = new Chicken();
    Duck duck = new Duck();
    Bird bird = duck;
    bird.swim();
    bird.fly();

    Animal animal = duck;
    animal.run();
    animal.eat();
  }
}