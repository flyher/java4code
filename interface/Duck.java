// 实现多个接口
class Duck implements Bird, Animal {
  // @Override
  public void swim() {
    System.out.println("i cannot swim!");
  }

  // @Override
  public void fly() {
    System.out.println("i can fly in the sky!");
  }

  public void run(){
    System.out.println("i can run!");
  };
  public void eat(){
    System.out.println("i want eat!");
  };
}