public class People {
    public String name;

    People(String name) {
        this.name = name;
        System.out.println("people constructor");
    }

    public void Say() {
        System.out.println("name:" + this.name);
    }
}