public class Student extends People {
    public int age;

    public Student(String name, int age) {
        // super();
        // 调用父类中的构造函数
        super(name);
        this.age = age;
        System.out.println("constructor");
    }

    // @Override
    public void Say() {
        System.out.println("i'm " + this.name);
    }
}