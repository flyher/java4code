class Test {
  public static void main(String[] args) {
    // uncheck exception
    try {
      int i = 1/0;
    } catch (Exception e) {
      //TODO: handle exception
      e.printStackTrace();
    } finally {
      System.out.println('finally')
    }
      
    // Check exception
    // try {
    //   Thread.sleep(1000);
    // } catch (Exception e) {
    //   //TODO: handle exception
    //   e.printStackTrace();
    // }
  }
}