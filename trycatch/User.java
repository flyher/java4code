class User {
  public String name;
  private int age;

  public void setAge(int age) throws Exception {
    if(age < 0){
      // 自定义异常
      // RuntimeException e = new RuntimeException("age can't be less than 0");
      Exception e = new Exception("age can't be less than 0"); // 抛出异常
      throw e; // 抛出异常
    }
    this.age = age;
  }
}