class Test {
  public static void main(String[] args) {
    // 根据用户的选择，生产相应的打印机对象
    // 并且向上转型位Printer类型
    // Printer getPrinter(int flag)
    // option 1
    // Printer printer =null;
    // int flag=1;
    // if(flag==0){
    //   printer = new APriter();
    // }else if (flag==1){
    //   printer = new BPriter();
    // }
    // printer.open();
    // printer.print();
    // printer.close();

    // option 2 :factory
    int flag=2;
    Printer printer = PrinterFactory.getPrinter(flag);
    printer.open();
    printer.print();
    printer.close();
  }
}