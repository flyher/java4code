class XXXPrinter implements Printer {
  public void open() {
    System.out.println("XXX open");
  }

  public void print() {
    System.out.println("XXX print");
  }

  public void close() {
    System.out.println("XXX close");
  }

}