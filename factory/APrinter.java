class APriter implements Printer {
  public void open() {
    System.out.println("A open");
  }

  public void print() {
    System.out.println("A print");
  }

  public void close() {
    System.out.println("A close");
  }

}