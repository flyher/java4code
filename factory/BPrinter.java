class BPriter implements Printer {
  public void open() {
    System.out.println("B open");
  }

  public void print() {
    System.out.println("B print");
  }

  public void close() {
    System.out.println("B close");
  }

  private void sendemail() {
    System.out.println("B send email");
  }
}