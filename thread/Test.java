class Test {
    public static void main(String[] args) {
        // 1 通过继承Thread来实现多线程（不常用的方式，java中只能继承一个）
        // // 生成线程类的对象
        // T1 t1=new T1();
        // // 启动线程
        // t1.start(); //start 方法来自Thread
        // // t1.run(); //这种是调用方法，不是执行线程

        // 2 通过实现接口来实现多线程
        //  生成T1的接口实现类的对象
        T2 t2 = new T2();
        // 生成一个Thread对象
        // 传递给该Thread对象
        Thread t = new Thread(t2);
        // 设置优先级
        t.setPriority(Thread.MAX_PRIORITY);
        // 通知Thread对象,执行start方法
        t.start();
        // 获取当前优先级
        System.out.println(t.getPriority());
    }
}