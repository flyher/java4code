class Test {
  public static void main(String[] args) {
    // 生成一个A公司水管工对象
    Plumber plumber = new Plumber();
    AWorker aWorker = new AWorker(plumber);
    aWorker.doSomeWork();
  }
}